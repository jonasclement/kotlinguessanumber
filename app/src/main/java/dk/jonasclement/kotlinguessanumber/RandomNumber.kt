package dk.jonasclement.kotlinguessanumber

import java.util.Random

/** Extension of Random.nextInt() that can take a range */
fun Random.nextInt(range: IntRange): Int {
    return range.start + nextInt(range.last - range.start)
}

/**
 * Generates a random number
 * @param preselectedNumber Number to pre-select. Intended for use in tests.
 */
class RandomNumber(preselectedNumber: Int? = null) {
    var number = 0

    init {
        number = preselectedNumber ?: generateNumber()
    }

    // TODO User should be able to set range
    private fun generateNumber(from: Int = 1, to: Int = 50): Int {
        if (from > to) throw IllegalArgumentException("'from' must be lower than 'to'")
        return Random().nextInt(from..to)
    }
}
