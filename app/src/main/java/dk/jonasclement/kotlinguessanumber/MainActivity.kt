package dk.jonasclement.kotlinguessanumber

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    /** The amount of times the user has guessed */
    private var guessCounter = 0
        set(value) {
        field = value
        text_main_header_part_2.text = value.toString()
        if (guessCounter == 1) {
            text_main_header_part_3.text = getString(R.string.main_time)
        } else {
            text_main_header_part_3.text = getString(R.string.main_times)
        }
    }
    /** The generated random number */
    private var generatedNumber = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Generate number
        generatedNumber = RandomNumber().number

        // Set on click listeners
        button_guess.setOnClickListener {
            try {
                val guess = Guess(textedit_guess.text.toString().toInt())
                guessCounter++

                // Set state text
                when {
                    guess.number == generatedNumber -> text_guess_state.text = getString(R.string.msg_correct)
                    guess.number < generatedNumber  -> text_guess_state.text = getString(R.string.msg_too_low)
                    guess.number > generatedNumber  -> text_guess_state.text = getString(R.string.msg_too_high)
                }

                if (guess.number == generatedNumber) win()
            } catch(e: NumberFormatException) {
                text_guess_state.text = getString(R.string.msg_invalid_guess)
            }
        }

        button_new_game.setOnClickListener {
            startNewGame()
        }
    }

    private fun showNewGameButton() {
        button_guess.visibility = View.GONE
        button_new_game.visibility = View.VISIBLE
    }

    private fun hideNewGameButton() {
        button_guess.visibility = View.VISIBLE
        button_new_game.visibility = View.GONE
    }

    private fun win() {
        showNewGameButton()
        text_main_header_part_2.setTextColor(ContextCompat.getColor(this, R.color.green))
        textedit_guess.isEnabled = false
    }

    private fun startNewGame() {
        // Reset values and view
        hideNewGameButton()
        textedit_guess.isEnabled = true
        textedit_guess.setText("")
        text_main_header_part_2.setTextColor(text_main_header_part_1.currentTextColor)
        guessCounter = 0
        generatedNumber = RandomNumber().number
    }
}
