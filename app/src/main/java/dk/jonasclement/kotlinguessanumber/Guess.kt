package dk.jonasclement.kotlinguessanumber

data class Guess(val number: Int)
