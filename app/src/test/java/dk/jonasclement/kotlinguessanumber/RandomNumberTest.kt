package dk.jonasclement.kotlinguessanumber

import org.junit.Assert.*
import org.junit.Test

const val PRESELECTED_NUMBER = 4
/** Tests for the RandomNumber class */
class RandomNumberTest {
    @Test fun preselectNumber() {
        val randomNumber = RandomNumber(PRESELECTED_NUMBER).number
        assertEquals(randomNumber, PRESELECTED_NUMBER)
    }
}
